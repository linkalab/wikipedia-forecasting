from urllib.parse import unquote
from datetime import datetime, timedelta
import time, logging, os, io, sys, traceback, csv
import psycopg2, psycopg2.extras
import json , pickle, re
import pandas as pd
import numpy as np
import psutil
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import r2_score
from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import EarlyStopping
from keras.optimizers import Adam
from keras.layers import LSTM, Dropout
from keras.preprocessing.sequence import TimeseriesGenerator

#* DataParameters
look_back  = 5
batch_size = 5
level_num  = 3

local_csv = './data/wikipedia_pageviews.csv'
local_old_prediction = './data/wikipedia_prediction_OLD.csv'
output_file_name = './data/wikipedia_prediction.csv'

def get_code(df, level):
    """
        It returns the list of codes corresponding to selected level.
        If the level is not corresponding to any code, it returns an empty list.
    """

    level_code = []
    for code in df.columns:
        if code.count('-') == level - 1:
            level_code.append(code)
    return level_code


def make_df(df_dict, third_level_code):
    df_new = df_dict[third_level_code[0]]
    df_new.rename(columns={0:third_level_code[0]}, inplace=True)
    for code in third_level_code[1:]:
        df_new = df_new.merge(df_dict[code], left_index=True, right_index= True)
        df_new.rename(columns = {0: code}, inplace=True)

    return df_new

def main():
    """
       Function to calculate the weekly pageviews predictions and its variations out of the pageviews data of previous weeks.
    """

    def get_el(pdata, i, elem):
        try:
            return pdata[elem][i][0]
        except:
            pass

        try:
            return pdata[elem][i]
        except:
            return None

    try:
        dfAll = getData([local_csv])

        third_level_code = get_code(dfAll, level = level_num)
        df_third_level = getThirdLevels(third_level_code, dfAll)
        df_new = make_df(df_third_level, third_level_code)

        try:
            dfOldPrediction = pd.read_csv(local_old_prediction, header=None)

            dfOnlyOldPred = dfOldPrediction[
                (dfOldPrediction.iloc[:,5].isnull()) #'dataset_prediction'
                & (dfOldPrediction.iloc[:,7].isnull()) #'inv_dataset_rescaled'
            ]

            # Get also older predictions
            dfOnlyOlderPred = dfOldPrediction[(dfOldPrediction.iloc[:,9].notnull()) & (dfOldPrediction.iloc[:,10].notnull())] #'dataset_orig'
        except:
            dfOnlyOldPred = pd.DataFrame()
            dfOnlyOlderPred = pd.DataFrame()

        ## Extract first 3 rows from the CSV: these will be used to create a dictionary keyed with the category
        ## and containing title and url. We also need the parent category title, so we loop again to fill it.
        additional_info = {}
        parents = {}
        with open(local_csv, "r") as lcsv:
            csvreader = list(csv.reader(lcsv))

            for i in range(len(csvreader[0])):
                if i == 0:
                    continue

                curr_cat = csvreader[1][i]

                # store the parents
                parent = curr_cat.split('-')[0]
                if parent == curr_cat:
                    parents[parent] = csvreader[0][i]

                additional_info[curr_cat] = {
                    'title': csvreader[0][i],
                    'parent': '',
                    'url': csvreader[2][i]
                }

        print(additional_info)

        ## loop through additional_info and fill the parent value
        for cat in additional_info:
            parent = cat.split('-')[0]
            additional_info[cat]['parent'] = parents[parent]

        results = []
        for code in third_level_code:
            weekdf = getWeekDF(df_new, code)
            weekdf.columns = ['data']

            ## get the data generator (keras TimeseriesGenerator)
            data_gen, dataset, scaler = prepareDataWeekly(weekdf.data, look_back, batch=batch_size)

            ## run the model (non verbose)
            model, history = getModel(data_gen, look_back)

            ## get prediction and variation
            datasetpr, variazione = getPredictionAndVariation(model, dataset, look_back)

            ## rescale back data to original scale
            rescaled_datasetpr = rescale(scaler, datasetpr)
            rescaled_dataset = rescale(scaler, dataset)

            ## prepare the data
            pdata = {
                'dataset_orig': dataset.tolist(),
                'dataset_with_prediction': datasetpr.tolist(),
                'dates': list(weekdf.index),
                'inv_rescaled_prediction': rescaled_datasetpr.tolist(),
                'inv_dataset': rescaled_dataset.tolist()
            }

            last_date = pdata['dates'][-1]

            for i in range(0, len(pdata['dataset_with_prediction'])):
                try:
                    date = str(pdata['dates'][i])
                except:
                    ## predictions are made for the next two weeks from the last week of data
                    last_date = datetime.strftime(datetime.strptime(last_date,"%Y-%m-%d") + timedelta(days=7), '%Y-%m-%d')
                    date = last_date

                dataset_orig = get_el(pdata, i, 'dataset_orig')
                dataset_prediction = get_el(pdata, i, 'dataset_with_prediction')
                inv_dataset = get_el(pdata, i, 'inv_dataset')
                inv_dataset_rescaled = get_el(pdata, i, 'inv_rescaled_prediction')

                if not dfOnlyOldPred.empty:
                    ## old and older prediction values
                    dfRowOldPred = dfOnlyOldPred[(dfOnlyOldPred.iloc[:,3]==additional_info[code]['title'])
                        & (dfOnlyOldPred.iloc[:,4]==date)]

                    dfRowOlderPred = dfOnlyOlderPred[(dfOnlyOlderPred.iloc[:,3]==additional_info[code]['title'])
                        & (dfOnlyOlderPred.iloc[:,4]==date)]

                    dataset_prediction_old = dfRowOlderPred.iloc[:,9].to_numpy()[0]
                    inv_dataset_rescaled_old = dfRowOlderPred.iloc[:,10].to_numpy()[0]
                else:
                    dataset_prediction_old = None
                    inv_dataset_rescaled_old = None

                results.append([
                    code,
                    additional_info[code]['parent'],
                    additional_info[code]['url'],
                    additional_info[code]['title'],
                    date,
                    dataset_orig,
                    dataset_prediction,
                    inv_dataset,
                    inv_dataset_rescaled,
                    dataset_prediction_old,
                    inv_dataset_rescaled_old
                ])

        print(results)

        with io.open(output_file_name, 'w', encoding='utf-8') as f:
            wr = csv.writer(f, quoting=csv.QUOTE_ALL, dialect='excel')
            wr.writerows(results)

    except Exception as e:
        print(e)

def selectCode(columns, code):
    """
        This function selects the columns to be aggregated, based on the taxonomy code.
        Hierarchy of taxonomy code: First level is represented by A, second level by A-B, third level by A-B-C and fourth level
        by A-B-C-D
    """
    try:
        cs = []
        for c in columns:
            if re.match("%s-D\d" % (code), c):
                cs.append(c)
        cs.insert(0, code)
        return cs
    except Exception as e:
        raise e


def getData(csvs):
    """
        This function reads the original csvs and returns a dataframe.
    """
    try:
        alldf = []
        dfAll = pd.DataFrame

        for csv in csvs:
            df = pd.read_csv(csv, skiprows=[0, 2])
            df = df.rename({df.columns[0]: 'date'}, axis=1)  # note the axis
            df.index = pd.to_datetime(df.date, format="%Y%m%d")  # convert to date the index
            alldf.append(df)
        dfAll = pd.concat(alldf, sort=False)

        return dfAll
    except Exception as e:
        raise e

def getThirdLevels(third_level_code, dfAll):
    """
        This functions groups the series starting from a code, and computes their median.
        A single category get associated to all the lower level categories. It groups then all the fields by median value.
    """
    try:
        df_third_level = {}
        for code in third_level_code:
            cs = selectCode(dfAll.columns, code)
            dfAll_r = dfAll[cs]
            dfM = dfAll_r.median(axis=1)
            df_third_level[code] = dfM.to_frame()
        return df_third_level
    except Exception as e:
        raise e

def aggregateWeek(df, timefield):
    """
        This function returns the weekly aggregation of the series.
    """
    try:
        wd = df[timefield].dt.weekday + 1  # +1 serve per la domenica
        # si creano il timedelta
        td = pd.to_timedelta(wd, unit='d')  # unità giorni

        ## valore con le ore
        dy = df[timefield] - td

        ## riformattato per solo giorni
        dyf = dy.dt.strftime('%Y-%m-%d')
        dfw = df.assign(weekd=dyf)
        weekdf = dfw.groupby('weekd').sum()
        return weekdf
    except Exception as e:
        raise e

def getWeekDF(dfAll, code):
    """
       It aggregates dataframes in week time slots.
    """
    try:
        dfc = dfAll[code]
        dfc = dfc.to_frame()
        dfc = dfc.assign(dtime=pd.to_datetime(dfc.index))
        weekdf = aggregateWeek(dfc, 'dtime')
        return weekdf
    except Exception as e:
        raise e

def prepareDataWeekly(data, look_back=5, batch=5):
    try:
        scaler = MinMaxScaler(feature_range=(0, 1))
        data = np.array(data).reshape(-1, 1)
        dataset = scaler.fit_transform(data)
        target = dataset
        data_gen = TimeseriesGenerator(
            dataset, target, length=look_back, sampling_rate=1, stride=1, batch_size=batch)
        return data_gen, dataset, scaler
    except Exception as e:
        raise e

def getModel(data_gen, look_back, verbose=False):
    try:
        model = Sequential()
        model.add(LSTM(4, input_shape=(look_back, 1)))
        model.add(Dense(1))
        model.compile(loss='mean_squared_error', optimizer='adam')
        history = model.fit_generator(data_gen, epochs=50, verbose=verbose).history
        return model, history
    except Exception as e:
        raise e

def getPredictionAndVariation(model, dataset, look_back):
    try:
        target_copy = dataset.copy()
        target_list = list(target_copy.flatten())
        last_value = target_copy[- look_back - 1: -1]
        last_value_r = np.array(last_value).reshape(1, look_back, 1)

        predictions = []
        for i in range(2):
            pr = model.predict(last_value_r)
            target_list.append(pr[0][0])
            last_value_r = np.array(
                target_list[- look_back - 1: -1]).reshape(1, look_back, 1)

            predictions.append(pr)
        datasetpr = np.append(dataset, predictions)

        variazione = 100. * (target_list[-1] - target_list[-3])  # % variazione
        return datasetpr, variazione
    except Exception as e:
        raise e

def rescale(scaler, data):
    try:
        r = scaler.inverse_transform(data.reshape(-1, 1))
        rr = [int(v) for v in r]
        return np.array(rr)
    except Exception as e:
        raise e


if __name__ == '__main__':
    main()