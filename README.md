# Wikipedia Forecasting

Function to calculate the weekly pageviews predictions (for the next 2 weeks) and its variations out of the pageviews data of previous weeks, using sklearn and keras/tensorflow.

## SETUP

Load required libraries:

```bash
~$ cd /this_path/
~$ python3 -m venv env
~$ . env/bin/activate
(env) ~$ pip install -r requirements.txt
```

then, execute the script:

```bash
(env) ~$ python3 prediction.py
```

The code will look inside the `data` folder loading pageviews from `wikipedia_pageviews.csv` and optionally also from previous forecastings inside the `wikipedia_prediction_OLD.csv`.

The pageviews file must contain at least a third level category in the form Ax-By-Cz (please see the file's contents); if you want to change this behaviour, please change the costant `level_num` inside the `prediction.py`.

Results will be saved on `data/wikipedia_prediction.csv`. This can be the "old" file for the next week.


# Authors

"Wikipedia Forecasting" is created by [Linkalab S.r.l.](http://www.linkalab.it)


# License

GNU General Public License v3.0 or later

See `LICENSE` to see the full text.